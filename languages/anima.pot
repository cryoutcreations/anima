msgid ""
msgstr ""
"Project-Id-Version: Anima 1.1\n"
"POT-Creation-Date: 2018-07-13 15:15+0200\n"
"PO-Revision-Date: 2018-07-13 15:16+0200\n"
"Last-Translator: Cryout Creations\n"
"Language-Team: Cryout Creations\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.5\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e;_x;_n;_n:1,2;esc_attr__;esc_attr_e;esc_html__\n"
"X-Poedit-SearchPath-0: .\n"

#: 404.php:15 includes/core.php:194 includes/core.php:409
msgid "Not Found"
msgstr ""

#: 404.php:16
msgid ""
"Apologies, but the page you requested could not be found. Perhaps searching "
"will help."
msgstr ""

#: comments.php:21 includes/comments.php:91 includes/comments.php:92
#, php-format
msgid "One Comment"
msgid_plural "%1$s Comments"
msgstr[0] ""
msgstr[1] ""

#: comments.php:45
msgid "Comments are closed."
msgstr ""

#: header.php:44
msgid "Primary Menu"
msgstr ""

#: image.php:31
msgid "pixels"
msgstr ""

#: image.php:38
msgid "Published in"
msgstr ""

#: image.php:65
msgid "Previous image"
msgstr ""

#: image.php:66
msgid "Next image"
msgstr ""

#: search.php:18 includes/core.php:191
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: searchform.php:11
msgid "Search for:"
msgstr ""

#: searchform.php:12 searchform.php:14 includes/core.php:427
msgid "Search"
msgstr ""

#: sidebar-left.php:18 includes/setup.php:55
msgid "Left Sidebar"
msgstr ""

#: sidebar-left.php:21 sidebar-right.php:21
#, php-format
msgid ""
"You currently have no widgets set in this sidebar. You can add widgets via "
"the <a href=\"%s\">Dashboard</a>."
msgstr ""

#: sidebar-left.php:22 sidebar-right.php:22
#, php-format
msgid ""
"To hide this sidebar, switch to a different Layout via the <a href=\"%s"
"\">Theme Customizations</a>."
msgstr ""

#: sidebar-right.php:18
msgid "Right Sidebar"
msgstr ""

#: single.php:45 content/content-page.php:34 content/content.php:41
msgid "Pages:"
msgstr ""

#: single.php:62
msgid "Previous Post"
msgstr ""

#: single.php:63
msgid "Next Post"
msgstr ""

#: admin/main.php:54
msgid "Reset Anima Settings to Defaults?"
msgstr ""

#: admin/main.php:62 admin/main.php:97
msgid "Anima Theme"
msgstr ""

#: admin/main.php:73
msgid "Sorry, but you do not have sufficient permissions to access this page."
msgstr ""

#: admin/main.php:82
msgid "Anima settings loaded successfully."
msgstr ""

#: admin/main.php:90
msgid "Anima settings have been reset successfully."
msgstr ""

#: admin/main.php:104 admin/options.php:101
msgid "Read the Docs"
msgstr ""

#: admin/main.php:105 admin/options.php:107
msgid "Browse the Forum"
msgstr ""

#: admin/main.php:106 admin/options.php:113
msgid "Priority Support"
msgstr ""

#: admin/main.php:119
#, php-format
msgid "Customize %s"
msgstr ""

#: admin/main.php:124 admin/options.php:125
msgid "Manage Theme Settings"
msgstr ""

#: admin/main.php:128
msgid "Reset to Defaults"
msgstr ""

#: admin/main.php:163
msgid "Theme Updates"
msgstr ""

#: admin/options.php:85
msgid "About"
msgstr ""

#: admin/options.php:88
msgid "Need Help?"
msgstr ""

#: admin/options.php:119
#, php-format
msgid "Rate %s on WordPress.org"
msgstr ""

#: admin/options.php:133
msgid "Background"
msgstr ""

#: admin/options.php:134
msgid "Background Settings."
msgstr ""

#: admin/options.php:141 admin/options.php:503
msgid "Header Image"
msgstr ""

#: admin/options.php:142
msgid "Header Image Settings."
msgstr ""

#: admin/options.php:149 admin/options.php:168
msgid "Site Identity"
msgstr ""

#: admin/options.php:169 admin/options.php:209 admin/options.php:463
msgid "Landing Page"
msgstr ""

#: admin/options.php:170
msgid "General"
msgstr ""

#: admin/options.php:171
msgid "Colors"
msgstr ""

#: admin/options.php:172
msgid "Typography"
msgstr ""

#: admin/options.php:173
msgid "Post Information"
msgstr ""

#: admin/options.php:182
msgid "Layout"
msgstr ""

#: admin/options.php:184 admin/options.php:207 admin/options.php:2294
msgid "Header"
msgstr ""

#: admin/options.php:186
msgid "Settings"
msgstr ""

#: admin/options.php:187 admin/options.php:497
msgid "Slider"
msgstr ""

#: admin/options.php:188
msgid "Featured Icon Blocks"
msgstr ""

#: admin/options.php:189
msgid "Featured Boxes"
msgstr ""

#: admin/options.php:190
msgid "Featured Boxes 2"
msgstr ""

#: admin/options.php:191
msgid "Text Areas"
msgstr ""

#: admin/options.php:193 admin/options.php:959
msgid "General Font"
msgstr ""

#: admin/options.php:194
msgid "Header Fonts"
msgstr ""

#: admin/options.php:195
msgid "Widget Fonts"
msgstr ""

#: admin/options.php:196
msgid "Content Fonts"
msgstr ""

#: admin/options.php:197
msgid "Formatting"
msgstr ""

#: admin/options.php:199
msgid "Structure"
msgstr ""

#: admin/options.php:200
msgid "Decorations"
msgstr ""

#: admin/options.php:201
msgid "Header Titles"
msgstr ""

#: admin/options.php:202
msgid "Content Images"
msgstr ""

#: admin/options.php:203
msgid "Search Box Locations"
msgstr ""

#: admin/options.php:204 includes/setup.php:57
msgid "Social Icons"
msgstr ""

#: admin/options.php:206
msgid "Content"
msgstr ""

#: admin/options.php:208 admin/options.php:2272
msgid "Footer"
msgstr ""

#: admin/options.php:211
msgid "Featured Image"
msgstr ""

#: admin/options.php:212
msgid "Meta Information"
msgstr ""

#: admin/options.php:213
msgid "Excerpts"
msgstr ""

#: admin/options.php:214 includes/comments.php:88
msgid "Comments"
msgstr ""

#: admin/options.php:216
msgid "Miscellaneous"
msgstr ""

#: admin/options.php:236
msgid "Main Layout"
msgstr ""

#: admin/options.php:239
msgid "One column (no sidebars)"
msgstr ""

#: admin/options.php:243
msgid "Two columns, sidebar on the right"
msgstr ""

#: admin/options.php:247
msgid "Two columns, sidebar on the left"
msgstr ""

#: admin/options.php:251
msgid "Three columns, sidebars on the right"
msgstr ""

#: admin/options.php:255
msgid "Three columns, sidebars on the left"
msgstr ""

#: admin/options.php:259
msgid "Three columns, one sidebar on each side"
msgstr ""

#: admin/options.php:268
msgid "Theme alignment"
msgstr ""

#: admin/options.php:270
msgid "Wide"
msgstr ""

#: admin/options.php:270 admin/options.php:755
msgid "Boxed"
msgstr ""

#: admin/options.php:276
msgid "Site Width"
msgstr ""

#: admin/options.php:283
msgid "Left Sidebar Width"
msgstr ""

#: admin/options.php:290
msgid "Right Sidebar Width"
msgstr ""

#: admin/options.php:298
msgid "Posts Layout"
msgstr ""

#: admin/options.php:301
msgid "One column"
msgstr ""

#: admin/options.php:305
msgid "Two columns"
msgstr ""

#: admin/options.php:309
msgid "Three columns"
msgstr ""

#: admin/options.php:318
msgid "Post/page padding"
msgstr ""

#: admin/options.php:327
msgid "Footer Widgets Columns"
msgstr ""

#: admin/options.php:330
msgid "All in a row"
msgstr ""

#: admin/options.php:331
msgid "1 Column"
msgstr ""

#: admin/options.php:332
msgid "2 Columns"
msgstr ""

#: admin/options.php:333
msgid "3 Columns"
msgstr ""

#: admin/options.php:334
msgid "4 Columns"
msgstr ""

#: admin/options.php:342 admin/options.php:1201
msgid "Default"
msgstr ""

#: admin/options.php:342 admin/options.php:387 admin/options.php:455
#: admin/options.php:1201
msgid "Center"
msgstr ""

#: admin/options.php:343
msgid "Footer Widgets Alignment"
msgstr ""

#: admin/options.php:352
msgid ""
"Fine tune the visibility of these elements in the theme's Header options"
msgstr ""

#: admin/options.php:364
msgid "Header/Menu Height"
msgstr ""

#: admin/options.php:371 admin/options.php:481 admin/options.php:504
#: admin/options.php:601 admin/options.php:632 admin/options.php:633
#: admin/options.php:651 admin/options.php:652 admin/options.php:670
#: admin/options.php:671 admin/options.php:689 admin/options.php:690
#: admin/options.php:715 admin/options.php:716 admin/options.php:801
#: admin/options.php:802 admin/options.php:809 admin/options.php:810
#: admin/options.php:817 admin/options.php:818 admin/options.php:825
#: admin/options.php:826 admin/options.php:1258 admin/options.php:1518
#: admin/options.php:1526
msgid "Disabled"
msgstr ""

#: admin/options.php:371 admin/options.php:465 admin/options.php:1518
#: admin/options.php:1526
msgid "Enabled"
msgstr ""

#: admin/options.php:372
msgid "Fixed Menu"
msgstr ""

#: admin/options.php:379
msgid "Normal"
msgstr ""

#: admin/options.php:379
msgid "Over header image"
msgstr ""

#: admin/options.php:380
msgid "Menu Position"
msgstr ""

#: admin/options.php:387 admin/options.php:455 admin/options.php:1201
msgid "Left"
msgstr ""

#: admin/options.php:387 admin/options.php:455 admin/options.php:1201
msgid "Right"
msgstr ""

#: admin/options.php:388
msgid "Menu Layout"
msgstr ""

#: admin/options.php:396
msgid "Header Image Height (in pixels)"
msgstr ""

#: admin/options.php:404 admin/options.php:1540 admin/options.php:1564
msgid "Changing this value may require to recreate your thumbnails."
msgstr ""

#: admin/options.php:411 admin/options.php:1547
msgid "Cropped"
msgstr ""

#: admin/options.php:411 admin/options.php:1547
msgid "Contained"
msgstr ""

#: admin/options.php:412
msgid "Header Image Behaviour"
msgstr ""

#: admin/options.php:418
msgid "Site Header Content"
msgstr ""

#: admin/options.php:420 admin/options.php:988
msgid "Site Title"
msgstr ""

#: admin/options.php:420
msgid "Logo"
msgstr ""

#: admin/options.php:420
msgid "Logo & Site Title"
msgstr ""

#: admin/options.php:420
msgid "Empty"
msgstr ""

#: admin/options.php:426
msgid "Show Tagline"
msgstr ""

#: admin/options.php:432
msgid "Logo Image"
msgstr ""

#: admin/options.php:441
msgid ""
"Edit the site's title, tagline and logo from WordPress' Site Identity panel."
msgstr ""

#: admin/options.php:446
msgid "Header Widget Width"
msgstr ""

#: admin/options.php:453
msgid "Header Widget Alignment"
msgstr ""

#: admin/options.php:465
msgid "Disabled (use WordPress homepage)"
msgstr ""

#: admin/options.php:473
#, php-format
msgid ""
"To activate the Landing Page, make sure to set the WordPress <strong>Front "
"Page displays</strong> option to %s"
msgstr ""

#: admin/options.php:473
msgid "use a static page"
msgstr ""

#: admin/options.php:479
msgid "Featured Content"
msgstr ""

#: admin/options.php:481
msgid "Static Page"
msgstr ""

#: admin/options.php:481 admin/options.php:1415
msgid "Posts"
msgstr ""

#: admin/options.php:488
msgid "More Posts Label"
msgstr ""

#: admin/options.php:500 admin/options.php:532
msgid "Serious Slider"
msgstr ""

#: admin/options.php:501
msgid "Use Shortcode"
msgstr ""

#: admin/options.php:502
msgid "Static Image"
msgstr ""

#: admin/options.php:506
#, php-format
msgid ""
"To create an advanced slider, use our <a href='%s' target='_blank'>Serious "
"Slider</a> plugin or any other slider plugin."
msgstr ""

#: admin/options.php:511
msgid "Slider Image"
msgstr ""

#: admin/options.php:512
msgid "The default image can be replaced by setting a new static image."
msgstr ""

#: admin/options.php:518
msgid "Slider Link"
msgstr ""

#: admin/options.php:525
msgid "Shortcode"
msgstr ""

#: admin/options.php:526
msgid ""
"Enter shortcode provided by slider plugin. The plugin will be responsible "
"for the slider's appearance."
msgstr ""

#: admin/options.php:534
msgid " - Please install, activate or update Serious Slider plugin - "
msgstr ""

#: admin/options.php:534
msgid " - No sliders defined - "
msgstr ""

#: admin/options.php:535
msgid ""
"Select the desired slider from the list. Sliders can be administered in the "
"dashboard."
msgstr ""

#: admin/options.php:541
msgid "Slider Caption"
msgstr ""

#: admin/options.php:543
msgid "Title"
msgstr ""

#: admin/options.php:550 admin/options.php:557 admin/options.php:571
msgid "Text"
msgstr ""

#: admin/options.php:555 admin/options.php:569
msgid "CTA Button"
msgstr ""

#: admin/options.php:564 admin/options.php:578
msgid "Link"
msgstr ""

#: admin/options.php:585 admin/options.php:700
msgid "Section Title"
msgstr ""

#: admin/options.php:592 admin/options.php:707
msgid "Section Description"
msgstr ""

#: admin/options.php:599
msgid "Blocks Content"
msgstr ""

#: admin/options.php:601
msgid "No Text"
msgstr ""

#: admin/options.php:601 admin/options.php:1474 admin/options.php:1490
msgid "Excerpt"
msgstr ""

#: admin/options.php:601
msgid "Full Content"
msgstr ""

#: admin/options.php:608 admin/options.php:780
msgid "Read More Button"
msgstr ""

#: admin/options.php:609
msgid "Configure the 'Read More' link text."
msgstr ""

#: admin/options.php:615
msgid "Make icons clickable (linking to their respective pages)."
msgstr ""

#: admin/options.php:622 admin/options.php:641 admin/options.php:660
#: admin/options.php:679
#, php-format
msgid "Block %d"
msgstr ""

#: admin/options.php:714
msgid "Boxes Content"
msgstr ""

#: admin/options.php:715 admin/options.php:716
msgid "All Categories"
msgstr ""

#: admin/options.php:727
msgid "Number of Boxes"
msgstr ""

#: admin/options.php:734
msgid "Boxes Per Row"
msgstr ""

#: admin/options.php:746
msgid "Box Height"
msgstr ""

#: admin/options.php:747
msgid ""
"In pixels. The width is a percentage dependent on total site width and "
"number of columns per row."
msgstr ""

#: admin/options.php:753
msgid "Box Layout"
msgstr ""

#: admin/options.php:755
msgid "Full width"
msgstr ""

#: admin/options.php:762
msgid "Box Stacking"
msgstr ""

#: admin/options.php:764
msgid "Joined"
msgstr ""

#: admin/options.php:764
msgid "Apart"
msgstr ""

#: admin/options.php:771
msgid "Box Appearance"
msgstr ""

#: admin/options.php:773
msgid "Animated"
msgstr ""

#: admin/options.php:773 admin/options.php:1242
msgid "Static"
msgstr ""

#: admin/options.php:791
msgid "Content Length (words)"
msgstr ""

#: admin/options.php:800 admin/options.php:808 admin/options.php:816
#: admin/options.php:824
#, php-format
msgid "Text Area %d"
msgstr ""

#: admin/options.php:827
msgid ""
"<br><br>Page properties that will be used:<br>- page title as text "
"title<br>- page content as text content<br>- page featured image as text "
"area background image"
msgstr ""

#: admin/options.php:836
msgid "Site Background"
msgstr ""

#: admin/options.php:842
msgid "Site Text"
msgstr ""

#: admin/options.php:848
msgid "Content Headings"
msgstr ""

#: admin/options.php:854
msgid "Content Background"
msgstr ""

#: admin/options.php:860
msgid "Left Sidebar Background"
msgstr ""

#: admin/options.php:866
msgid "Right Sidebar Background"
msgstr ""

#: admin/options.php:872
msgid "Overlay Color"
msgstr ""

#: admin/options.php:878
msgid "Overlay Opacity"
msgstr ""

#: admin/options.php:885
msgid "Header Background"
msgstr ""

#: admin/options.php:891
msgid "Menu Text"
msgstr ""

#: admin/options.php:897
msgid "Submenu Text"
msgstr ""

#: admin/options.php:903
msgid "Submenu Background"
msgstr ""

#: admin/options.php:909
msgid "Footer Background"
msgstr ""

#: admin/options.php:915
msgid "Footer Text"
msgstr ""

#: admin/options.php:921
msgid "Slider Background"
msgstr ""

#: admin/options.php:927
msgid "Blocks Background"
msgstr ""

#: admin/options.php:933
msgid "Boxes Background"
msgstr ""

#: admin/options.php:939
msgid "Text Areas Background"
msgstr ""

#: admin/options.php:945
msgid "Primary Accent"
msgstr ""

#: admin/options.php:951
msgid "Secondary Accent"
msgstr ""

#: admin/options.php:968 admin/options.php:997 admin/options.php:1026
#: admin/options.php:1055 admin/options.php:1084 admin/options.php:1113
#: admin/options.php:1142 admin/options.php:1172
msgid "100 thin"
msgstr ""

#: admin/options.php:968 admin/options.php:997 admin/options.php:1026
#: admin/options.php:1055 admin/options.php:1084 admin/options.php:1113
#: admin/options.php:1142 admin/options.php:1172
msgid "200 extra-light"
msgstr ""

#: admin/options.php:968 admin/options.php:997 admin/options.php:1026
#: admin/options.php:1055 admin/options.php:1084 admin/options.php:1113
#: admin/options.php:1142 admin/options.php:1172
msgid "300 ligher"
msgstr ""

#: admin/options.php:968 admin/options.php:997 admin/options.php:1026
#: admin/options.php:1055 admin/options.php:1084 admin/options.php:1113
#: admin/options.php:1142 admin/options.php:1172
msgid "400 regular"
msgstr ""

#: admin/options.php:968 admin/options.php:997 admin/options.php:1026
#: admin/options.php:1055 admin/options.php:1084 admin/options.php:1113
#: admin/options.php:1142 admin/options.php:1172
msgid "500 medium"
msgstr ""

#: admin/options.php:968 admin/options.php:997 admin/options.php:1026
#: admin/options.php:1055 admin/options.php:1084 admin/options.php:1113
#: admin/options.php:1142 admin/options.php:1172
msgid "600 semi-bold"
msgstr ""

#: admin/options.php:968 admin/options.php:997 admin/options.php:1026
#: admin/options.php:1055 admin/options.php:1084 admin/options.php:1113
#: admin/options.php:1142 admin/options.php:1172
msgid "700 bold"
msgstr ""

#: admin/options.php:968 admin/options.php:997 admin/options.php:1026
#: admin/options.php:1055 admin/options.php:1084 admin/options.php:1113
#: admin/options.php:1142 admin/options.php:1172
msgid "800 extra-bold"
msgstr ""

#: admin/options.php:968 admin/options.php:997 admin/options.php:1026
#: admin/options.php:1055 admin/options.php:1084 admin/options.php:1113
#: admin/options.php:1142 admin/options.php:1172
msgid "900 black"
msgstr ""

#: admin/options.php:981
msgid ""
"The fonts under the <em>Preferred Theme Fonts</em> list are recommended "
"because they have all the font weights used throughout the theme."
msgstr ""

#: admin/options.php:982 admin/options.php:1011 admin/options.php:1040
#: admin/options.php:1069 admin/options.php:1098 admin/options.php:1127
#: admin/options.php:1156 admin/options.php:1186
msgid "or enter Google Font Identifier"
msgstr ""

#: admin/options.php:1017
msgid "Main Menu"
msgstr ""

#: admin/options.php:1046
msgid "Widget Title"
msgstr ""

#: admin/options.php:1075
msgid "Widget Content"
msgstr ""

#: admin/options.php:1104
msgid "Post/Page Titles"
msgstr ""

#: admin/options.php:1133
msgid "Post metas"
msgstr ""

#: admin/options.php:1163
msgid "Headings"
msgstr ""

#: admin/options.php:1192
msgid "Line Height"
msgstr ""

#: admin/options.php:1199
msgid "Text Alignment"
msgstr ""

#: admin/options.php:1201
msgid "Justify"
msgstr ""

#: admin/options.php:1207
msgid "Paragraph Spacing"
msgstr ""

#: admin/options.php:1214
msgid "Paragraph Indentation"
msgstr ""

#: admin/options.php:1224
msgid "Breadcrumbs"
msgstr ""

#: admin/options.php:1226 admin/options.php:1234 admin/options.php:1573
#: admin/options.php:1611 admin/options.php:1619 admin/options.php:1627
#: admin/options.php:1635 admin/options.php:1643
msgid "Enable"
msgstr ""

#: admin/options.php:1226 admin/options.php:1234 admin/options.php:1242
#: admin/options.php:1573 admin/options.php:1611 admin/options.php:1619
#: admin/options.php:1627 admin/options.php:1635 admin/options.php:1643
msgid "Disable"
msgstr ""

#: admin/options.php:1232
msgid "Numbered Pagination"
msgstr ""

#: admin/options.php:1240
msgid "Single Post Prev/Next Navigation"
msgstr ""

#: admin/options.php:1242
msgid "Absolute"
msgstr ""

#: admin/options.php:1248
msgid "Page/Category Titles"
msgstr ""

#: admin/options.php:1250
msgid "Always Visible"
msgstr ""

#: admin/options.php:1250
msgid "Hide on Pages"
msgstr ""

#: admin/options.php:1250
msgid "Hide on Categories"
msgstr ""

#: admin/options.php:1250
msgid "Always Hidden"
msgstr ""

#: admin/options.php:1256
msgid "Back to Top Button"
msgstr ""

#: admin/options.php:1258
msgid "Bottom of page"
msgstr ""

#: admin/options.php:1258
msgid "In footer"
msgstr ""

#: admin/options.php:1264
msgid "Tables Style"
msgstr ""

#: admin/options.php:1266
msgid "No border"
msgstr ""

#: admin/options.php:1266
msgid "Clean"
msgstr ""

#: admin/options.php:1266
msgid "Stripped"
msgstr ""

#: admin/options.php:1266
msgid "Bordered"
msgstr ""

#: admin/options.php:1272
msgid "Tags Cloud Appearance"
msgstr ""

#: admin/options.php:1274
msgid "Size Emphasis"
msgstr ""

#: admin/options.php:1274
msgid "Uniform Boxes"
msgstr ""

#: admin/options.php:1280
msgid "Custom Footer Text"
msgstr ""

#: admin/options.php:1281
msgid ""
"Insert custom text or basic HTML code that will appear in your footer. <br /"
"> You can use HTML to insert links, images and special characters."
msgstr ""

#: admin/options.php:1289
msgid "Border"
msgstr ""

#: admin/options.php:1295
msgid "Shadow"
msgstr ""

#: admin/options.php:1301
msgid "Rounded Corners"
msgstr ""

#: admin/options.php:1302
msgid "These decorations apply to certain theme elements."
msgstr ""

#: admin/options.php:1307
msgid "Article Animation on Scroll"
msgstr ""

#: admin/options.php:1309
msgid "None"
msgstr ""

#: admin/options.php:1309
msgid "Fade"
msgstr ""

#: admin/options.php:1309
msgid "Slide"
msgstr ""

#: admin/options.php:1309
msgid "Slide Left"
msgstr ""

#: admin/options.php:1309
msgid "Slide Right"
msgstr ""

#: admin/options.php:1318
msgid "Add Search to Main Menu"
msgstr ""

#: admin/options.php:1324
msgid "Add Search to Footer Menu"
msgstr ""

#: admin/options.php:1333
msgid "Post Images"
msgstr ""

#: admin/options.php:1336
msgid "No Styling"
msgstr ""

#: admin/options.php:1340 admin/options.php:1344 admin/options.php:1348
#: admin/options.php:1352 admin/options.php:1356
#, php-format
msgid "Style %d"
msgstr ""

#: admin/options.php:1365
msgid "Post Captions"
msgstr ""

#: admin/options.php:1367
msgid "Plain"
msgstr ""

#: admin/options.php:1367
msgid "With Border"
msgstr ""

#: admin/options.php:1367
msgid "With Background"
msgstr ""

#: admin/options.php:1377
msgid "Display Author"
msgstr ""

#: admin/options.php:1383
msgid "Display Date"
msgstr ""

#: admin/options.php:1389
msgid "Display Time"
msgstr ""

#: admin/options.php:1395
msgid "Display Category"
msgstr ""

#: admin/options.php:1401
msgid "Display Tags"
msgstr ""

#: admin/options.php:1407
msgid "Display Comments"
msgstr ""

#: admin/options.php:1408
msgid "Choose meta information to show on posts."
msgstr ""

#: admin/options.php:1421
msgid "Pages"
msgstr ""

#: admin/options.php:1427
msgid "Archive pages"
msgstr ""

#: admin/options.php:1433
msgid "Home page"
msgstr ""

#: admin/options.php:1441
msgid "'Comments Are Closed' Text"
msgstr ""

#: admin/options.php:1443
msgid "Show"
msgstr ""

#: admin/options.php:1443
msgid "Hide in posts"
msgstr ""

#: admin/options.php:1443
msgid "Hide in pages"
msgstr ""

#: admin/options.php:1443
msgid "Hide everywhere"
msgstr ""

#: admin/options.php:1449
msgid "Comment Date Format"
msgstr ""

#: admin/options.php:1451
msgid "Specific"
msgstr ""

#: admin/options.php:1451
msgid "Relative"
msgstr ""

#: admin/options.php:1457
msgid "Comment Field Label"
msgstr ""

#: admin/options.php:1459
msgid "Placeholders"
msgstr ""

#: admin/options.php:1459
msgid "Labels"
msgstr ""

#: admin/options.php:1460
msgid "Change to labels for better compatibility with comment-related plugins."
msgstr ""

#: admin/options.php:1465
msgid "Comment Form Width (pixels)"
msgstr ""

#: admin/options.php:1472
msgid "Standard Posts On Homepage"
msgstr ""

#: admin/options.php:1474 admin/options.php:1482 admin/options.php:1490
msgid "Full Post"
msgstr ""

#: admin/options.php:1475
msgid "Post formats always display full posts."
msgstr ""

#: admin/options.php:1480
msgid "Sticky Posts on Homepage"
msgstr ""

#: admin/options.php:1482
msgid "Inherit"
msgstr ""

#: admin/options.php:1488
msgid "Standard Posts in Categories/Archives"
msgstr ""

#: admin/options.php:1496
msgid "Excerpt Length (words)"
msgstr ""

#: admin/options.php:1502
msgid "Excerpt Suffix"
msgstr ""

#: admin/options.php:1508
msgid "Continue Reading Label"
msgstr ""

#: admin/options.php:1516
msgid "Featured Images"
msgstr ""

#: admin/options.php:1524
msgid "Auto Select Image From Content"
msgstr ""

#: admin/options.php:1532
msgid "Featured Image Height (in pixels)"
msgstr ""

#: admin/options.php:1533
msgid "Set to 0 to disable image processing"
msgstr ""

#: admin/options.php:1548
msgid "Featured Image Behaviour"
msgstr ""

#: admin/options.php:1549
msgid ""
"<strong>Contained</strong> will scale depending on the viewed "
"resolution<br><strong>Cropped</strong> will always have the configured "
"height."
msgstr ""

#: admin/options.php:1554
msgid "Featured Image Crop Position"
msgstr ""

#: admin/options.php:1556
msgid "Left Top"
msgstr ""

#: admin/options.php:1556
msgid "Left Center"
msgstr ""

#: admin/options.php:1556
msgid "Left Bottom"
msgstr ""

#: admin/options.php:1556
msgid "Right Top"
msgstr ""

#: admin/options.php:1556
msgid "Right Center"
msgstr ""

#: admin/options.php:1556
msgid "Right Bottom"
msgstr ""

#: admin/options.php:1556
msgid "Center Top"
msgstr ""

#: admin/options.php:1556
msgid "Center Center"
msgstr ""

#: admin/options.php:1556
msgid "Center Bottom"
msgstr ""

#: admin/options.php:1571
msgid "Use Featured Images in Header"
msgstr ""

#: admin/options.php:1582
msgid "Display in Header"
msgstr ""

#: admin/options.php:1588
msgid "Display in Footer"
msgstr ""

#: admin/options.php:1594
msgid "Display in Left Sidebar"
msgstr ""

#: admin/options.php:1600
msgid "Display in Right Sidebar"
msgstr ""

#: admin/options.php:1601
#, php-format
msgid ""
"Select where social icons should be visible in.<br><br><strong>Social Icons "
"are defined using the <a href=\"%1$s\" target=\"_blank\">social icons menu</"
"a></strong>. Read the <a href=\"%2$s\" target=\"_blank\">theme "
"documentation</a> on how to create a social menu."
msgstr ""

#: admin/options.php:1609
msgid "Masonry"
msgstr ""

#: admin/options.php:1617
msgid "JS Defer loading"
msgstr ""

#: admin/options.php:1625
msgid "FitVids"
msgstr ""

#: admin/options.php:1627
msgid "Enable on mobiles"
msgstr ""

#: admin/options.php:1633
msgid "Autoscroll"
msgstr ""

#: admin/options.php:1641
msgid "Editor Styles"
msgstr ""

#: admin/options.php:1644
msgid "<br>Only use these options to troubleshoot issues."
msgstr ""

#: admin/options.php:2258
msgid "Sidebar Left"
msgstr ""

#: admin/options.php:2265
msgid "Sidebar Right"
msgstr ""

#: admin/options.php:2273
msgid ""
"You can configure how many columns the footer displays from the theme options"
msgstr ""

#: admin/options.php:2280
msgid "Content Before"
msgstr ""

#: admin/options.php:2287
msgid "Content After"
msgstr ""

#: content/author-bio.php:30
msgid "View all posts by "
msgstr ""

#: content/content-notfound.php:10
msgid "Nothing Found"
msgstr ""

#: content/content-notfound.php:12
#, php-format
msgid "No search results for: <em>%s</em>"
msgstr ""

#: cryout/admin-functions.php:25
msgid "No update news."
msgstr ""

#: cryout/admin-functions.php:29
msgid "Posted on"
msgstr ""

#: cryout/admin-functions.php:30
msgid "Read the full post"
msgstr ""

#: cryout/back-compat.php:32 cryout/back-compat.php:42
#: cryout/back-compat.php:55
#, php-format
msgid ""
"%1$s requires at least WordPress version 4.1. You are running version %2$s. "
"Please upgrade and try again."
msgstr ""

#: cryout/tgmpa-class.php:327
msgid "Install Required Plugins"
msgstr ""

#: cryout/tgmpa-class.php:328
msgid "Install Plugins"
msgstr ""

#: cryout/tgmpa-class.php:330 includes/tgmpa.php:55
#, php-format
msgid "Installing Plugin: %s"
msgstr ""

#: cryout/tgmpa-class.php:332 includes/tgmpa.php:57
#, php-format
msgid "Updating Plugin: %s"
msgstr ""

#: cryout/tgmpa-class.php:333 includes/tgmpa.php:58
msgid "Something went wrong with the plugin API."
msgstr ""

#: cryout/tgmpa-class.php:385
msgid "Return to Required Plugins Installer"
msgstr ""

#: cryout/tgmpa-class.php:386 cryout/tgmpa-class.php:827
#: cryout/tgmpa-class.php:2533 cryout/tgmpa-class.php:3580
msgid "Return to the Dashboard"
msgstr ""

#: cryout/tgmpa-class.php:387 cryout/tgmpa-class.php:3159
#: includes/tgmpa.php:111
msgid "Plugin activated successfully."
msgstr ""

#: cryout/tgmpa-class.php:388 cryout/tgmpa-class.php:2952
#: includes/tgmpa.php:112
msgid "The following plugin was activated successfully:"
msgid_plural "The following plugins were activated successfully:"
msgstr[0] ""
msgstr[1] ""

#: cryout/tgmpa-class.php:390 includes/tgmpa.php:114
#, php-format
msgid "No action taken. Plugin %1$s was already active."
msgstr ""

#: cryout/tgmpa-class.php:392 includes/tgmpa.php:116
#, php-format
msgid ""
"Plugin not activated. A higher version of %s is needed for this theme. "
"Please update the plugin."
msgstr ""

#: cryout/tgmpa-class.php:394 includes/tgmpa.php:118
#, php-format
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: cryout/tgmpa-class.php:395 includes/tgmpa.php:119
msgid "Dismiss this notice"
msgstr ""

#: cryout/tgmpa-class.php:396 includes/tgmpa.php:120
msgid ""
"There are one or more required or recommended plugins to install, update or "
"activate."
msgstr ""

#: cryout/tgmpa-class.php:397 includes/tgmpa.php:121
msgid "Please contact the administrator of this site for help."
msgstr ""

#: cryout/tgmpa-class.php:522
msgid "This plugin needs to be updated to be compatible with your theme."
msgstr ""

#: cryout/tgmpa-class.php:523
msgid "Update Required"
msgstr ""

#: cryout/tgmpa-class.php:934
msgid ""
"The remote plugin package does not contain a folder with the desired slug "
"and renaming did not work."
msgstr ""

#: cryout/tgmpa-class.php:934 cryout/tgmpa-class.php:937
msgid ""
"Please contact the plugin provider and ask them to package their plugin "
"according to the WordPress guidelines."
msgstr ""

#: cryout/tgmpa-class.php:937
msgid ""
"The remote plugin package consists of more than one file, but the files are "
"not packaged in a folder."
msgstr ""

#: cryout/tgmpa-class.php:1982
#, php-format
msgid "TGMPA v%s"
msgstr ""

#: cryout/tgmpa-class.php:2273
msgid "Required"
msgstr ""

#: cryout/tgmpa-class.php:2276
msgid "Recommended"
msgstr ""

#: cryout/tgmpa-class.php:2292
msgid "WordPress Repository"
msgstr ""

#: cryout/tgmpa-class.php:2295
msgid "External Source"
msgstr ""

#: cryout/tgmpa-class.php:2298
msgid "Pre-Packaged"
msgstr ""

#: cryout/tgmpa-class.php:2315
msgid "Not Installed"
msgstr ""

#: cryout/tgmpa-class.php:2319
msgid "Installed But Not Activated"
msgstr ""

#: cryout/tgmpa-class.php:2321
msgid "Active"
msgstr ""

#: cryout/tgmpa-class.php:2327
msgid "Required Update not Available"
msgstr ""

#: cryout/tgmpa-class.php:2330
msgid "Requires Update"
msgstr ""

#: cryout/tgmpa-class.php:2333
msgid "Update recommended"
msgstr ""

#: cryout/tgmpa-class.php:2342
#, php-format
msgid "%1$s, %2$s"
msgstr ""

#: cryout/tgmpa-class.php:2392
#, php-format
msgid "To Install <span class=\"count\">(%s)</span>"
msgid_plural "To Install <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: cryout/tgmpa-class.php:2396
#, php-format
msgid "Update Available <span class=\"count\">(%s)</span>"
msgid_plural "Update Available <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: cryout/tgmpa-class.php:2400
#, php-format
msgid "To Activate <span class=\"count\">(%s)</span>"
msgid_plural "To Activate <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: cryout/tgmpa-class.php:2482
msgid "unknown"
msgstr ""

#: cryout/tgmpa-class.php:2490
msgid "Installed version:"
msgstr ""

#: cryout/tgmpa-class.php:2498
msgid "Minimum required version:"
msgstr ""

#: cryout/tgmpa-class.php:2510
msgid "Available version:"
msgstr ""

#: cryout/tgmpa-class.php:2533
msgid "No plugins to install, update or activate."
msgstr ""

#: cryout/tgmpa-class.php:2547
msgid "Plugin"
msgstr ""

#: cryout/tgmpa-class.php:2548
msgid "Source"
msgstr ""

#: cryout/tgmpa-class.php:2549
msgid "Type"
msgstr ""

#: cryout/tgmpa-class.php:2553
msgid "Version"
msgstr ""

#: cryout/tgmpa-class.php:2554
msgid "Status"
msgstr ""

#: cryout/tgmpa-class.php:2603
#, php-format
msgid "Install %2$s"
msgstr ""

#: cryout/tgmpa-class.php:2608
#, php-format
msgid "Update %2$s"
msgstr ""

#: cryout/tgmpa-class.php:2614
#, php-format
msgid "Activate %2$s"
msgstr ""

#: cryout/tgmpa-class.php:2684
msgid "Upgrade message from the plugin author:"
msgstr ""

#: cryout/tgmpa-class.php:2717
msgid "Install"
msgstr ""

#: cryout/tgmpa-class.php:2723
msgid "Update"
msgstr ""

#: cryout/tgmpa-class.php:2726
msgid "Activate"
msgstr ""

#: cryout/tgmpa-class.php:2757
msgid "No plugins were selected to be installed. No action taken."
msgstr ""

#: cryout/tgmpa-class.php:2759
msgid "No plugins were selected to be updated. No action taken."
msgstr ""

#: cryout/tgmpa-class.php:2800
msgid "No plugins are available to be installed at this time."
msgstr ""

#: cryout/tgmpa-class.php:2802
msgid "No plugins are available to be updated at this time."
msgstr ""

#: cryout/tgmpa-class.php:2908
msgid "No plugins were selected to be activated. No action taken."
msgstr ""

#: cryout/tgmpa-class.php:2934
msgid "No plugins are available to be activated at this time."
msgstr ""

#: cryout/tgmpa-class.php:3158
msgid "Plugin activation failed."
msgstr ""

#: cryout/tgmpa-class.php:3498
#, php-format
msgid "Updating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: cryout/tgmpa-class.php:3501
#, php-format
msgid "An error occurred while installing %1$s: <strong>%2$s</strong>."
msgstr ""

#: cryout/tgmpa-class.php:3503
#, php-format
msgid "The installation of %1$s failed."
msgstr ""

#: cryout/tgmpa-class.php:3507
msgid ""
"The installation and activation process is starting. This process may take a "
"while on some hosts, so please be patient."
msgstr ""

#: cryout/tgmpa-class.php:3509
#, php-format
msgid "%1$s installed and activated successfully."
msgstr ""

#: cryout/tgmpa-class.php:3509 cryout/tgmpa-class.php:3517
msgid "Show Details"
msgstr ""

#: cryout/tgmpa-class.php:3509 cryout/tgmpa-class.php:3517
msgid "Hide Details"
msgstr ""

#: cryout/tgmpa-class.php:3510
msgid "All installations and activations have been completed."
msgstr ""

#: cryout/tgmpa-class.php:3512
#, php-format
msgid "Installing and Activating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: cryout/tgmpa-class.php:3515
msgid ""
"The installation process is starting. This process may take a while on some "
"hosts, so please be patient."
msgstr ""

#: cryout/tgmpa-class.php:3517
#, php-format
msgid "%1$s installed successfully."
msgstr ""

#: cryout/tgmpa-class.php:3518
msgid "All installations have been completed."
msgstr ""

#: cryout/tgmpa-class.php:3520
#, php-format
msgid "Installing Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: includes/comments.php:25
msgid "Pingback: "
msgstr ""

#: includes/comments.php:25 includes/comments.php:61
msgid "(Edit)"
msgstr ""

#: includes/comments.php:36
msgid "Your comment is awaiting moderation."
msgstr ""

#: includes/comments.php:53
msgid "at"
msgstr ""

#: includes/comments.php:56
#, php-format
msgid "%1$s ago"
msgstr ""

#: includes/comments.php:66
msgid "Reply"
msgstr ""

#: includes/comments.php:90
msgid "Leave a comment"
msgstr ""

#: includes/comments.php:120 includes/comments.php:121
msgid "Name"
msgstr ""

#: includes/comments.php:124 includes/comments.php:125
msgid "Email"
msgstr ""

#: includes/comments.php:128 includes/comments.php:129
msgid "Website"
msgstr ""

#: includes/comments.php:133
msgid ""
"Save my name, email, and site URL in my browser for next time I post a "
"comment."
msgstr ""

#: includes/comments.php:142
msgid "Comment"
msgstr ""

#: includes/core.php:262
msgid "Back to Top"
msgstr ""

#: includes/core.php:312
msgid "Powered by"
msgstr ""

#: includes/core.php:315
msgid "Semantic Personal Publishing Platform"
msgstr ""

#: includes/core.php:404
msgid "Home"
msgstr ""

#: includes/core.php:405
msgid "Archive for category"
msgstr ""

#: includes/core.php:406
msgid "Search results for"
msgstr ""

#: includes/core.php:407
msgid "Posts tagged"
msgstr ""

#: includes/core.php:408
msgid "Articles posted by"
msgstr ""

#: includes/core.php:410
msgid "Post format"
msgstr ""

#: includes/core.php:411
msgid "Page"
msgstr ""

#: includes/loop.php:85
msgid "Categories"
msgstr ""

#: includes/loop.php:103
msgid "Author"
msgstr ""

#: includes/loop.php:109 includes/loop.php:136
#, php-format
msgid "View all posts by %s"
msgstr ""

#: includes/loop.php:123
msgid "on"
msgstr ""

#: includes/loop.php:130
msgid "Posted by "
msgstr ""

#: includes/loop.php:160
msgid "Date"
msgstr ""

#: includes/loop.php:183
msgid "Tagged"
msgstr ""

#: includes/loop.php:195
#, php-format
msgid "Edit %s"
msgstr ""

#: includes/loop.php:206
msgid "Featured"
msgstr ""

#: includes/loop.php:257
msgid "Older posts"
msgstr ""

#: includes/loop.php:261
msgid "Newer posts"
msgstr ""

#: includes/meta.php:18
msgid "Static Page Layout"
msgstr ""

#: includes/meta.php:40
msgid "Default Theme Layout"
msgstr ""

#: includes/setup.php:54
msgid "Primary Navigation"
msgstr ""

#: includes/setup.php:56
msgid "Footer Navigation"
msgstr ""

#: includes/setup.php:120
msgid "glasses"
msgstr ""

#: includes/setup.php:125
msgid "laptop"
msgstr ""

#: includes/setup.php:175
msgid "Skip to content"
msgstr ""

#: includes/tgmpa.php:52
msgid "Anima Suggested Plugins"
msgstr ""

#: includes/tgmpa.php:53
msgid "Anima Addons"
msgstr ""

#: includes/tgmpa.php:110
msgid "Return to Suggested Plugins Installer"
msgstr ""
